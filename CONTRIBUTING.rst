Contribuindo
============

Perguntas ou sugestões serem enviadas criadas em
https://gitlab.com/rgaiacs/software-na-academia-brasileira/issues
ou por email para incoming+rgaiacs/software-na-academia-brasileira@gitlab.com.

Outras contribuições são bem-vindas desde que respeitadas as regras abaixo.

1. Contribuições serem licenciadas sob a licença MIT.
   Para maiores detalhes verificar :doc:`LICENSE`.

2. Contribuições serem submetidas via https://gitlab.com/rgaiacs/software-na-academia-brasileira/merge_requests.

Recomendações
-------------

1. Utilize português.
