Sugestão de referências
=======================

- S. Hettrick, "It's impossible to conduct research without software, say 7 out
  of 10 UK researchers," Software Sustainaiblity Institute, 2014. Available at: https://www.software.ac.uk/blog/2016-09-12-itsimpossible-conduct-research-without-software-say-7-out-10-ukresearchers.
- S. J. Hettrick, M. Antonioletti, L. Carr, N. Chue Hong, S. Crouch, D. De Roure, et al, “UK Research Software Survey 2014”, Zenodo, 2014. doi:10.5281/zenodo.14809.
- Nangia, Udit, & Katz, Daniel S. (2017). Track 1 Paper: Surveying the U.S. National Postdoctoral Association Regarding Software Use and Training in Research. Zenodo. http://doi.org/10.5281/zenodo.814220
- Gizmodo, “Survey Response Rates,” 2015. Available from: https://www.surveygizmo.com/survey-blog/survey-response-rates/.
