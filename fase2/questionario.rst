Questionário
============

Estamos realizando esse questionário
para identificar a importância de programas de computador científico
no processo de gerar, processar ou analisar resultados que serão incluídos em uma publicação.
Para tanto,
gostaríamos da sua colaboração, preenchendo esse questionário.
O tempo estimado de preenchimento é **5 minutos**.

Em caso de dúvidas quanto ao preenchimento favor enviar e-mail para
`incoming+rgaiacs/software-na-academia-brasileira@gitlab.com <mailto:incoming+rgaiacs/software-na-academia-brasileira@gitlab.com>`_.

Termos e condições
------------------

- Você está respondendo esse questionário voluntariamente.
- Esse questionário não pede nenhuma informação pessoal que permita identificá-lo.
- Os dados coletados serão disponibilizados em https://zenodo.org/.

1. Você concorda com os termos e condições anteriores?

   - Sim
   - Não

O que é programa de computador científico?
------------------------------------------

Para esse questionário, definimos "programa de computador científico" como

    Programa de computador que é utilizado para gerar, processar ou analisar
    resultados que você pretende incluir em uma publicação
    (seja em um periódico, conferência, monografia, dissertação, tese ou livro).

    Pode ser desde
    algumas poucas linha de código escritas por você,
    um pacote desenvolvido por um profissional
    ou qualquer coisa entre esses dois extremos.

    Programa de computador que **não** gera, processa ou analisa resultados
    (como, por exemplo, processadores de texto ou buscadores)
    não contam como "programa de computador científico" para esta pesquisa.

2. Você utiliza programa de computador científico?

   - Sim
   - Não

3. Quais os programas de computador científicos que você utiliza com maior frequência?

Treinamento
-----------

4. Você obteve algum treinamento nos programas de computador científicos que utiliza,
   ou aprendeu a usá-los por conta própria (usando livros e/ou material online, por exemplo)?

   - Obtive treinamento em todos os programas de computador científicos que utilizo.
   - Obtive treinamento em por volta de 95% dos programas de computador científicos que utilizo.
   - Obtive treinamento em por volta de 75% dos programas de computador científicos que utilizo.
   - Obtive treinamento em por volta de 50% dos programas de computador científicos que utilizo.
   - Obtive treinamento em por volta de 25% dos programas de computador científicos que utilizo.
   - Obtive treinamento em por volta de 5% dos programas de computador científicos que utilizo.
   - Não obtive nenhum treinamento.

5. Se você obteve algum treinamento,
   quem ministrou o treinamento, na maioria das vezes?

   - Criador do programa de computador científico.
   - Parceiro oficial do criador/da empresa do programa de computador científico.
   - Empresa, instituição ou indivíduo especializado nesse tipo de treinamento.
   - Minicurso em congresso ou evento científico.
   - Algum colega que usa o mesmo programa.
   - Outro. Especifique:

Desenvolvimento de Programa de Computador
-----------------------------------------

6. Você desenvolve ou desenvolveu pelo menos um dos programas de computador científicos que utiliza?

   - Sim
   - Não

7. Você recebeu algum treinamento em metodologia para o desenvolvimento de programa de computador?

   - Sim, participei de um curso, presencial ou online, ou workshop sobre o tema.
   - Não, sou autodidata no desenvolvimento dos meus programas de computador científicos.
   - Não se aplica.

Pesquisa
--------

8. O que aconteceria se você não mais pudesse utilizar um dos programas de computador
   científicos que utiliza hoje?

   - Não faria diferença na minha pesquisa.
   - Minha pesquisa precisaria de mais horas de trabalho, mas ainda seria
     possível realizá-la.
   - Não seria prático conduzir minha pesquisa sem os programas de computador dos quais
     disponho hoje.

9. Você já incluiu o custo do programa de computador científico ou de seu desenvolvimento
   que utiliza em um projeto de pesquisa?

    - Sim, para compra e desenvolvimento do programa de computador científico.
    - Sim, para compra do programa de computador científico.
    - Sim, para o desenvolvimento do programa de computador científico.
    - Não.

Demografia
----------

10. Em qual estágio de carreira você está?

   - aluno/aluna de graduação
   - aluno/aluna de mestrado
   - aluno/aluna de doutorado
   - pós-doutorando/pós-doutoranda
   - professor/professora
   - pesquisador/pesquisadora (caso nenhuma das anteriores se aplique)
   - prefiro não informar

Obrigado
--------

Agradecemos por ter respondido esse questionário.

Os resultados desse questionário serão disponibilizados em https://rgaiacs.gitlab.io/software-na-academia-brasileira/.
