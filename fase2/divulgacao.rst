Divulgação do questionário
==========================

Ao divulgar o questionário,
ter maior controle sobre
(1) número total de indivíduos contactados,
(2) número de individuos que "viram" o convite,
(3) número de indiviúdos que responderam o questionário.
Para (2), uma opção é utilizar link de rastreamento no email.
