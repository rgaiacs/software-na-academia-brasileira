Fase 1
======

Nessa primeira fase,
obteremos experiência ao circular o questionário
entre os pesquisadores de um departamento ou instituto.

Calendário
----------

- 12/06 - Concluir questionário
- 12/06 - Primeira rodada de divulgação do questionário
- 29/06 - Segunda e última rodada de divulgação do questionário
- 03/07 - Salvar respostas dos questionários em https://gitlab.com/rgaiacs/software-na-academia-brasileira
- 07/07 - Finalizar primeira versão do artigo
- 10/07 - Submeter artigo para http://wssspe.researchcomputing.org.uk/wssspe5-1/

Documentos
----------

.. toctree::
   :maxdepth: 1

   questionario

Dados
-----

.. admonition:: Como Citar

   Silva, Raniere (2017): Track 1 Lightning Talk: Research Software in Brazil - Dataset. figshare. https://doi.org/10.6084/m9.figshare.5328052.v1

:download:`Baixe os dados coletados no questionário em CSV <ResearchSoftwareSurvey2017RawPT.csv>`.

Os dados coletados encontram-se também em inglês. A "tradução" foi feita utilizando :download:`esse script <pt2en.sh>`.
:download:`Baixe a versão em inglês <ResearchSoftwareSurvey2017RawPT.csv>`.

Análise
-------

.. admonition:: Como Citar

  Silva, Raniere (2017): Track 1 Lightning Talk: Research Software in Brazil - Jupyter Notebook. figshare. https://doi.org/10.6084/m9.figshare.5328058.v2

A análise dos dados foi feita em Python.
O Jupyter Notebook com a análise **em inglês** encontra-se disponível :download:`aqui <analyse.ipynb>`
e uma versão para visualisação em HTML :download:`aqui <analyse.html>`.

Relatório
---------

.. admonition:: Como Citar

   Silva, Raniere (2017): Track 1 Lightning Talk: Research Software in Brazil. figshare. https://doi.org/10.6084/m9.figshare.5328043.v1

O relatório (**em inglês**) encontra-se disponível :download:`aqui <paper.pdf>`.
