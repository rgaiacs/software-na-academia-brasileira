cp ResearchSoftwareSurvey2017Raw{PT,EN}.csv

sed -i "s/Você concorda com os termos e condições anteriores?/Do you agree with the terms and conditions?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Você utiliza programa de computador científico?/Do you use research software?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Quais os programas de computador científico que você utiliza com maior frequência?/Please provide the name(s) of the main research software you use./" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Você obteve algum treinamento nos programas de computador científico que utiliza, ou aprendeu a usá-los por conta própria (usando livros e\/ou material online, por exemplo)?/Have you received any training in research software that you use?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Não obtive nenhum treinamento./I didn't receive any training./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Obtive treinamento em por volta de 25% dos programas de computador científico que utilizo./I received training for 25% of the research software that I use./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Obtive treinamento em por volta de 5% dos programas de computador científico que utilizo./I received training for 5% of the research software that I use./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Obtive treinamento em por volta de 50% dos programas de computador científico que utilizo./I received training for 50% of the research software that I use./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Obtive treinamento em por volta de 75% dos programas de computador científico que utilizo./I received training for 75% of the research software that I use./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Obtive treinamento em todos os programas de computador científico que utilizo./I received training for 100% of the research software that I use./" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Se você obteve algum treinamento, quem ministrou o treinamento, na maioria das vezes?/If you received any training, who did provide the training?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Algum colega que usa o mesmo programa./With a colleague that use the same program./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Aprendi sozinho ou com colegas mais experientes//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Aprendi sozinha//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Aprendi sozinho//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Conta própria//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Empresa, instituição ou indivíduo especializado nesse tipo de treinamento./Training company./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Este campo não deveria ser obrigatório.//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Eu proprio com livros e internet.//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Faltou a opção de não tive treinamento//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Minicurso em congresso ou evento científico./Workshop during a scientific conference./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/N\/A//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/NA//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Ninguém//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não obtive nenhum treinamento!!!//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não obtive nenhum treinamento.//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não obtive treinamento//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não obtive//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Só//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/internet\/conta própria//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/não obtive nenhum treinamento//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/,-,/,,/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Você desenvolve ou desenvolveu pelo menos um dos programas de computador científico que utiliza?/Do you develop your own research software?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Você recebeu algum treinamento em metodologia para o desenvolvimento de programa de computador?/Have you received any training in software development?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Não se aplica.//" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não, sou autodidata no desenvolvimento dos meus programas de computador./No/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Sim, participei de um curso, presencial ou online, ou workshop sobre o tema./Yes/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/O que aconteceria se você não mais pudesse utilizar um dos programas de computador científico que utiliza hoje/What would happen if you could no longer use research software?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Minha pesquisa precisaria de mais horas de trabalho, mas ainda seria possível realizá-la./My work would require more effort, but it would still be possible/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não faria diferença na minha pesquisa./It would make no significant difference to my work/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não seria prático conduzir minha pesquisa sem os programas de computador dos quais disponho hoje./It would not be practical to conduct my work without software/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Você já incluiu o custo do programa de computador científico ou de seu desenvolvimento que utiliza em um projeto de pesquisa?/Have you ever included the cost of research software in a funding proposal?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Sim, para compra do programa de computador científico./Yes, to purchase a research software./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Sim, para compra e desenvolvimento do programa de computador científico./Yes, to purchase and develop a research software./" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Sim, para o desenvolvimento do programa de computador científico./Yes, to develop a research software./" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Em qual estágio de carreira você está?/What is your job title?/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/aluno\/aluna de doutorado/PhD student/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/aluno\/aluna de graduação/Master student/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/aluno\/aluna de mestrado/Undergraduate student/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/pesquisador\/pesquisadora (caso nenhuma das anteriores se aplique)/Post Doctoral/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/pesquisador\/pesquisadora (caso nenhuma das anteriores se aplique)/Post Doctoral/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/prefiro não informar/Unknown/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/professor\/professora/Professor/" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/pós-doutorando\/pós-doutoranda/Post Doctoral/" ResearchSoftwareSurvey2017RawEN.csv

sed -i "s/Sim/Yes/g" ResearchSoftwareSurvey2017RawEN.csv
sed -i "s/Não/No/g" ResearchSoftwareSurvey2017RawEN.csv
